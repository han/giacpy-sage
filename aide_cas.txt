domain
smith
wz_certificate
keep_algext
jacobi_linsolve
gauss_seidel_linsolve
linspace
besselY
BesselY
besselJ
BesselJ
batons
()
REDIM redim
REPLACE replace
linfnorm
frobenius_norm
matrix_norm
fadeev
COND cond
bezier
flatten
chisquaret
plotcdf
back
find
index
count
uniform uniformd
uniform_cdf uniformd_cdf
uniform_icdf uniformd_icdf
exponential exponentiald
exponential_cdf exponentiald_cdf
exponential_icdf exponentiald_icdf
geometric
geometric_cdf
geometric_icdf
weibull weibulld
weibull_cdf weibulld_cdf
weibull_icdf weibulld_icdf
cauchy cauchyd
cauchy_cdf cauchyd_cdf
cauchy_icdf cauchyd_icdf
cdf
icdf
markov
randmarkov
conjugate_gradient
mgf
kolmogorovd
kolmogorovt
gammad
gammad_cdf
gammad_icdf
betad
betad_cdf
betad_icdf
taux_accroissement
shift_phase
revert
Heaviside
Dirac
polar_coordinates
rectangular_coordinates
polar_point
vector
residue
even
odd
tdeg
plex
revlex
unfactored sans_factoriser
DOM_FLOAT float double
DOM_INT integer DOM_int
DOM_SYMBOLIC symbol expression
DOM_IDENT identifier
DOM_LIST vector
DOM_STRING string
DOM_COMPLEX complex
DOM_FUNC func
DOM_RAT rational
set[] %{%}
i[] [..]
complexroot
posubLMQ
poslbdLMQ
VAS_positive
VAS
realroot
rationalroot
crationalroot
trapezoid middle_point left_rectangle right_rectangle trapeze point_milieu rectangle_droit rectangle_gauche
simpson rombergt rombergm gauss15
pow2exp
expexpand
lnexpand
trigexpand
combine
plotarea areaplot
plotinequation inequationplot
plotdensity densityplot
frames trames
animate
animate3d
animation
camembert
bar_plot
listplot plotlist
pari
bitor
bitand
bitxor
hamdist
simplex_reduce
spline
convexhull
as_function_of
save_history
DispG
DispHome
ans
quest entry
ClrIO
ithprime
nprimes
assign
ismith
ihermite
GF
droite_tangente
LineTan
xor
reverse_rsolve
revlist reverse
ratnormal
pade
is_coplanar
is_cospherical
is_included
gnuplot
current_sheet
lll
fft
ifft
hessenberg
SCHUR schur
companion
cote
chrem
ecris
signe
prism
common_perpendicular
point2d
point3d
octahedron
icosahedron
dodecahedron
parallelepiped
polyhedron
pyramid tetrahedron
cube
centered_tetrahedron
centered_cube
cone
half_cone
cylinder
erase3d
sphere
plane
line
point
efface
si alors sinon fsi
pour
de from
jusque to
tantque
do faire
return
dessine_tortue
avance forward
recule backward
saute
pas_de_cote
tourne_droite
tourne_gauche
leve_crayon penup
baisse_crayon pendown
crayon pencolor
position
cap heading
vers
rond
disque
disque_centre
rectangle_plein
triangle_plein
polygone_rempli
repete
debut_enregistrement
fin_enregistrement
cache_tortue
montre_tortue
envelope
powerpc
cross_ratio
division_point
radical_axis
pole
polar
reciprocation
harmonic_conjugate
harmonic_division
is_conjugate
is_harmonic
is_harmonic_circle_bundle
is_harmonic_line_bundle
is_orthogonal
is_isosceles
is_equilateral
is_rectangle
is_square
is_parallelogram
is_rhombus
coordinates
false FALSE False
true TRUE True
or || ou
and && et
end end_for fpour end_while ftantque end_if fi fsi od ffaire ffonction ffunction
bisection_solver brent_solver falsepos_solver newton_solver secant_solver steffenson_solver
dnewton_solver hybrid_solver hybrids_solver hybridj_solver hybridsj_solver newtonj_solver
_cm
_mm
_km
_m^2
_m^3
_cm^2
_cm^3
_km^2
_yd^2
_yd^3
_ft^2
_ft^3
_in^2
_in^3
_mi^2
_miUS^2
_ml
_s
_mn
_(m/s)
_(cm/s)
_(ft/s)
_(m/s^2)
_Wh
_kWh
_Kcal
_MeV
_(ft*lb)
_tep
_lep
_bblep
_MW
_degreeF
_deg
_Rankine
_MHz
_tec
_tr
_tex
_(rad/s)
_(tr/min)
_(tr/s)
_Gal
_(rad/s^2)
_NA_
_k_
_Vm_
_R_
_StdT_
_StdP_
_sigma_
_c_
_epsilon0_
_mu0_
_g_
_G_
_h_
_hbar_
_qe_
_me_
_qme_
_mp_
_mpme_
_alpha_
_phi_
_F_
_Rinfinity_
_RSun_
_PSun_
_mEarth_
_REarth_
_sd_
_syr_
_a0_
_muB_
_muN_
_lambda0_
_f0_
_lambdac_
_rad_
_c3_
_kq_
_epsilon0q_
_qepsilon0_
_epsilonsi_
_epsilonox_
_a
_A
_acre
_arcmin
_arcs
_atm
_au
_Angstrom
_b
_bar
_bbl
_Bq
_Btu
_bu
_buUS
_cal
_cd
_chain
_Ci
_ct
_cu
_d
_j
_dB
_dyn
_erg
_eV
_F
_fm
_fath
_fbm
_fc
_Fdy
_fermi
_flam
_ft
_ftUS
_g
_ga
_galUS
_galC
_galUK
_gf
_gmol
_grad
_gon
_grain
_Gy
_H
_ha
_h
_hp
_Hz
_in
_inHg
_inH20
_J
_K
_kg
_kip
_knot
_kph
_l
_lam
_lb
_lbf
_lbmol
_lbt
_lm
_lx
_lyr
_m
_µ
_mho
_mile
_mil
_miUS
_mmHg
_mol
_mph
_N
_mille
_Ohm
_oz
_ozfl
_ozt
_ozUK
_P
_Pa
_pc
_pdl
_ph
_pk
_psi
_liqpt
_ptUK
_qt
_rad
_R
_rd
_rod
_rem
_rpm
_s
_S
_sb
_slug
_sr
_st
_St
_Sv
_t
_T
_tbsp
_therm
_ton
_tonUK
_torr
_tsp
_u
_V
_W
_Wb
_yd
_yr
grid_paper
line_paper
triangle_paper
dot_paper
ufactor
usimplify
Celsius2Fahrenheit
Fahrenheit2Celsius
mksa
CST
isopolygon
arc
makesuite
makevector
interval2center
center2interval
border
JordanBlock
blockmatrix
row
col
delrows
delcols
frequencies
cumulated_frequencies
is_element
NewPic
RclPic
RplcPic
non_recursive_normal
evalc
copy
zip
unquote
time
DropDown
Request
Text
Title
Dialog EndDlog
choosebox PopUp
rdiv
unapply
RandSeed
semi_augment
goto
label
lsmod
insmod
rmmod
widget_size
CyclePic
StoPic
SortA
SortD
sorta
sortd
restart
product mul
maple_ifactors
.+
.-
./
.*
.^
&^
^
matpow
&*
+
-
+&
inputform
#
fMax
fMin
DrawInv
cFactor cfactor factoriser_sur_C
atrig2ln
LineHorz
Line
LineVert
plot3d graphe3d
subMat
table
read ramene
write sauve
csv2gen
nodisp
subsop
mRowAdd
scaleadd SCALEADD
mRow 
scale SCALE
mathml
export_mathml
xml_print
fracmod iratrecon
Beta
cat
compare
Col
Row
heugcd
modgcd
psrgcd
ezgcd
icontent
polygonplot ligne_polygonale
polygonscatterplot ligne_polygonale_pointee
scatterplot nuage_points
user_operator
count_eq
count_inf
count_sup
root
latex TeX
plotcontour contourplot DrwCtour
Circle
version
range
seq
$
roots
pivot
cZeros
srand randseed
powexpand
mult_c_conjugate multiplier_conjugue_complexe
mult_conjugate multiplier_conjugue
comDenom
cumSum cumsum
normalize unitV
l1norm
Int
dfc2f
dfc
det_minor
rm_all_vars
rm_a_z
risch
rowAdd
rowSwap swaprow rowswap SWAPROW
colSwap swapcol colswap SWAPCOL
variance
erf
erfc
reduced_conic
reduced_quadric
parallelogram
subs
simult
Pause sleep
monotonic
WAIT
part
newList
newMat
QR
LQ
LU
SVD
SVL
LSQ lsq
iPart
trunc
fPart frac
output Output
input Input lis
InputStr textinput lis_phrase
getType
getKey
assert
str string
list
polynom
denom
getDenom
getNum
numer
DrawSlp
findhelp ?
Fill
expr execute
list2exp
exp2list
ClrGraph ClrDraw
NewFold
SetFold
DelFold
GetFold
avgRC
nDeriv
Archive
Unarchiv
archive
unarchive
CopyVar
csolve cSolve resoudre_dans_C
arcLen arclen
cas_setup
about
abscissa
multinomial
binomial
binomial_cdf
binomial_icdf
negbinomial
negbinomial_cdf
negbinomial_icdf
colspace
@@
@
angle_radian
approx_mode
complex_mode
complex_variables
variables_are_files
all_trig_solutions
with_sqrt
increasing_power
Digits DIGITS
function_diff fonction_derivee
id
ordinate
poisson_cdf
poisson_icdf
poisson
rowspace
sq
linear_interpolate
logistic_regression
logistic_regression_plot
linear_regression
linear_regression_plot
logarithmic_regression
logarithmic_regression_plot
polynomial_regression
polynomial_regression_plot
power_regression
power_regression_plot
parfrac fullparfrac
confrac
interval
convert convertir
base
array
homogeneize
lcoeff
tcoeff
list2mat
mat2list
deltalist
multiply
accumulate_head_tail
square
hexagon
rhombus
polygon
open_polygon
quadrilateral
rectangle
Airy_Ai
Airy_Bi
cycleinv
groupermu
perminv
permu2mat
permuorder
chisquare chisquared
chisquare_cdf chisquared_cdf
chisquare_icdf chisquared_icdf
fisher snedecor fisherd snedecord
fisher_cdf snedecor_cdf fisherd_cdf snedecord_cdf
fisher_icdf snedecor_icdf fisherd_icdf snedecord_icdf
normald NORMALD
normal_cdf normald_cdf
normal_icdf normald_icdf
student studentd
student_cdf
student_icdf
UTPC
UTPF
UTPN
UTPT
classes
primpart
content
genpoly
left lhs gauche
right rhs droit
ord
rotate
shift
correlation
covariance
covariance_correlation
exponential_regression
exponential_regression_plot
histogram
boxwhisker moustache
mean moyenne
median
quantile
quartile1
quartile3
quartiles
stddev ecart_type
stddevp stdDev ecart_type_population
sqrfree
inv inverse
Inverse
rowdim nrows rowDim
coldim ncols colDim
identity idn
BlockDiagonal
diag 
upper
lower
coeff coeffs
taylor
dim
format
abs
acos arccos
acosh arccosh ACOSH
acot ACOT
acsc ACSC
adjoint_matrix
affix
algvar
angle
angleat
angleatraw
distanceat
distanceatraw
slope
slopeat
slopeatraw
area
areaat
areaatraw
perimeter
perimeterat
perimeteratraw
extract_measure
append
arg
args
asin arcsin ASIN
asc
asec ASEC
asinh arcsinh ASINH
assume
additionally
at
atan arctan ATAN
atan2acos
atan2asin
atanh arctanh ATANH
a2q
backquote
basis
bisector
exbisector
bloc
begin
bernoulli
break
breakpoint
canonical_form
case
catch
cd
ceil ceiling
center
orthocenter
changebase
char chr
continue
cont
chinrem
conic
quadric
circle
cholesky
circumcircle
click
comb nCr
comment
concat augment extend
conj
inString
member
contains
cos COS
cosh COSH
cot COT
cross crossproduct crossP
csc CSC
curl
curve
cycle2perm
cycles2permu
cyclotomic
c1oc2
c1op2
debug
default otherwise
degree
total_degree
desolve deSolve dsolve
det
Det
divergence
divpc
dot dotP scalarProduct scalar_product dotprod
*
diff derive deriver
'
symb2poly e2r
poly2symb r2e
egcd gcdex
egv eigenvectors eigenvects eigVc
egvl eigVl
eigenvals eigenvalues
else
elif
element
ellipse
epsilon2zero
=
equal
equal2diff
equal2list
equation
erase
error ERROR throw
is_collinear
is_concyclic
is_cycle
is_parallel
is_permu est_permu
is_perpendicular
euler Phi
euler_gamma
eval evalm
evalf approx
evalb
exp EXP
factor factoriser
Factor
factor_xn
factorial
fcoeff
expand fdistrib developper
for
feuille op
float2rational exact
floor
froot
fsolve
cfsolve
nSolve
Gamma
ugamma
igamma
LambertW
gauss
Gcd
gcd igcd
gramschmidt
graph2tex
graph3d2tex
hadamard
halftan_hyp2exp
halt
has
altitude
barycenter
isobarycenter
median_line
perpen_bisector
midpoint
head
hermite
smith
grad
hessian
hilbert
homothety
inversion
hyp2exp
hyperbola
i
e
pi Pi
ibasis
infinity
+infinity inf
-infinity
ibpdv
ibpu
ichinrem ichrem
if
ifactor factoriser_entier
ecm_factor
ilaplace invlaplace
im imag
image
in
incircle
excircle
int integrate integrer
inter
single_inter line_inter
iquo intDiv
/%
%/
div
iquorem divmod
irem remain
%/
is_pseudoprime
is_prime
isprime isPrime
isom
lgcd
jacobi_symbol
jordan
rat_jordan
ker kernel nullspace
Nullspace
Resultant
kill tuer
lagrange interp
laguerre
laplace
laplacian
lcm
legendre
locus
lin lineariser
linsolve resoudre_systeme_lineaire
thickness epaisseur
axes
style
title titre
gl_texture
gl_shownames
gl_showaxes
gl_quaternion
gl_rotation
gl_ortho
gl_x gl_y gl_z
gl_xtick gl_ytick gl_ztick
gl_x_axis_unit gl_y_axis_unit gl_z_axis_unit
gl_x_axis_color gl_y_axis_color gl_z_axis_color
gl_x_axis_name gl_y_axis_name gl_z_axis_name
legend
labels
legendre_symbol
ln log LN
expln
local var
logb
log10
alog10
distance
distance2
limit limite
lu
lvar
lname indets
norm l2norm
maxnorm
rowNorm rownorm
colNorm colnorm
max
makelist
map
apply
xcas_mode maple_mode
python_compat
maple2mupad
maple2xcas
mupad2maple
mupad2xcas
min
mkisom
minus
moyal
newton
nextprime
normal evala
not non
!
nop
odesolve
of
option
order_size
parabola
parallel
parameter
plotparam paramplot DrawParm courbe_parametrique
parameq
partfrac
cpartfrac
pcar charpoly
pcar_hessenberg
perm nPr
perpendicular
orthogonal
proot
pcoeff pcoef
peval polyEval
plot graphe
plotfunc funcplot DrawFunc Graph
plotimplicit implicitplot
fieldplot plotfield
interactive_plotode interactive_odeplot
odeplot plotode
pmin
order
plotpolar polarplot DrawPol courbe_polaire
rgb
set_pixel draw_pixel
draw_line
draw_rectangle
draw_polygon
draw_circle
draw_arc
clear
show_pixels
pixon
pixoff
potential
powmod powermod
pow
preval
prevprime
prepend
print Disp
printf
projection
program
proc
Psi
ptayl
purge DelVar del
pwd
p1oc2
p1op2
qr
quote hold
quo
Quo
quorem divide
q2a
rand random
choice
sample
randint
hasard
randbinomial
randmultinomial
randgeometric
randpoisson
randchisquare
randstudent
randfisher
randnorm randNorm normalvariate
randexp expovariate
randgammad gammavariate
randweibulld weibullvariate
randbetad betavariate
randpoly randPoly
ranm randMat randmatrix
randvector ranv
random_variable randvar
randperm shuffle
nextperm
prevperm
radius
re real
rem
Rem
remove
reorder
resultant
sylvester
rootof
rotation
rmbreakpoint
rmwatch
rref gaussjord
keep_pivot
Rref
ref
same
sec
segment
half_line
series
select
sign
signature
sin SIN
sinh
similarity
size nops length len
sizes
zeros
solve resoudre
line_segments
faces
vertices vertices_abc
vertices_abca
sommet
smod mods
sqrt
surd
sst
sst_in
stack
step by pas
sto Store
<=
>=
>
<
==
!=
:=
=<
+=
*=
-=
/=
=>
|
subst substituer
sum somme add
suppress
insert
svd
switch
piecewise
switch_axes
xyztrange
Ox_2d_unit_vector
Oy_2d_unit_vector
Ox_3d_unit_vector
Oy_3d_unit_vector
Oz_3d_unit_vector
frame_2d
frame_3d
reflection
syst2mat
tabvar
tabsign
tablefunc
tableseq
seqsolve
rsolve
tan TAN
tangent tangente
tchebyshev1
tchebyshev2
test
then
tlin lineariser_trigo
trace
tran transpose
translation
triangle
equilateral_triangle
isosceles_triangle
right_triangle
trn
truncate
try
type
subtype
union
valuation ldegree
vandermonde
VARS
vpotential
watch
while
repeat until repeter jusqua jusqu_a
{}
abcuv
iabcuv
iegcd igcdex bezout_entiers
acos2asin
acos2atan
asin2acos
asin2atan
collect
idivis divisors
divis
exp2pow
exp2trig
sincos
ifactors facteurs_premiers
factors
fourier_an
fourier_bn
fourier_cn
f2nd fxnd
gbasis
gbasis_max_pairs
gbasis_simult_primes
gbasis_reinject
greduce
halftan
horner
ifte IFTE
when
?
intersect
lncollect
makemat
matrix
display color
point_width_1 point_width_2 point_width_3 point_width_4 point_width_5 point_width_6 point_width_7 epaisseur_point_1 epaisseur_point_2 epaisseur_point_3 epaisseur_point_4 epaisseur_point_5 epaisseur_point_6 epaisseur_point_7
line_width_1 line_width_2 line_width_3 line_width_4 line_width_5 line_width_6 line_width_7 epaisseur_ligne_1 epaisseur_ligne_2 epaisseur_ligne_3 epaisseur_ligne_4 epaisseur_ligne_5 epaisseur_ligne_6 epaisseur_ligne_7
dash_line ligne_tiret solid_line ligne_trait_plein dashdot_line ligne_tiret_point dashdotdot_line ligne_tiret_pointpoint cap_flat_line ligne_chapeau_plat cap_square_line ligne_chapeau_carre cap_round_line ligne_chapeau_rond
rhombus_point point_losange plus_point point_plus square_point point_carre cross_point point_croix triangle_point point_triangle star_point point_etoile point_point invisible_point point_invisible
hidden_name nom_cache
filled
white black red green blue yellow magenta cyan
quadrant1 quadrant2 quadrant3 quadrant4
-<
->
function fonction	
mod %
fmod
pa2b2
permu2cycles
plotseq seqplot graphe_suite
propfrac propFrac
rank
romberg nInt
gaussquad
round
epsilon
simp2
simplify simplifier
sort sorted
split
sum_riemann
sturmseq
sturm
sturmab
tail
mid
tan2sincos
sin2costan
cos2sintan
tan2sincos2
tan2cossin2
tanh
tcollect tCollect rassembler_trigo
texpand tExpand developper_transcendant
trig2exp
trigcos
trigsin
trigtan
tsimplify
Zeta
open
fopen
fclose close
fprint
readwav
writewav
playsnd
soundsec
readrgb
writergb
Li
Ei
Ci
Si
invztrans
ztrans
eval_level
eliminate
algsubs
plotproba
autosimplify 
regroup
is_inside
in_ideal
normalt
studentt
osculating_circle
curvature
evolute
wilcoxont
wilcoxonp
wilcoxons
dayofweek
minimize maximize
minimax
tpsolve
thiele
lpsolve
nlpsolve
implicitdiff
lp_assume lp_binary lp_binaryvariables lp_depthlimit lp_integer lp_integervariables lp_maximize lp_nonnegative lp_nonnegint lp_variables lp_nodelimit lp_integertolerance lp_method lp_simplex lp_interiorpoint lp_initialpoint lp_maxcuts lp_gaptolerance lp_nodeselect lp_varselect lp_firstfractional lp_lastfractional lp_mostfractional lp_pseudocost lp_depthfirst lp_breadthfirst lp_bestprojection lp_hybrid lp_iterationlimit lp_timelimit lp_verbose
periodic
extrema
createwav
stereo2mono
bit_depth
channels
channel_data
duration
samplerate
resample
plotwav
plotspectrum
boxcar
rect
tri
sinc
fourier
ifourier
addtable
cross_correlation
auto_correlation
convolution
lowpass
highpass
moving_average
threshold
bartlett_hann_window
blackman_harris_window
blackman_window
bohman_window
cosine_window
gaussian_window
hamming_window
hann_poisson_window
hann_window
parzen_window
poisson_window
riemann_window
triangle_window
tukey_window
welch_window
trigsimplify
triginterp
heapify
heappush
heappop
trail
graph
digraph
export_graph
import_graph
graph_vertices
edges
has_edge
has_arc
adjacency_matrix
incidence_matrix
weight_matrix
graph_complement
subgraph
vertex_degree
vertex_in_degree
vertex_out_degree
induced_subgraph
maximal_independent_set
maximum_matching
make_directed
underlying_graph
cycle_graph
lcf_graph
hypercube_graph
seidel_switch
draw_graph
sierpinski_graph
complete_graph
petersen_graph
random_graph
random_digraph
random_bipartite_graph
random_tournament
random_regular_graph
random_sequence_graph
random_tree
random_planar_graph
assign_edge_weights
articulation_points
biconnected_components
add_arc
delete_arc
add_edge
delete_edge
add_vertex
delete_vertex
contract_edge
connected_components
departures
arrivals
incident_edges
make_weighted
set_graph_attribute
get_graph_attribute
discard_graph_attribute
set_vertex_attribute
get_vertex_attribute
discard_vertex_attribute
set_edge_attribute
get_edge_attribute
discard_edge_attribute
list_graph_attributes
list_vertex_attributes
list_edge_attributes
number_of_edges
number_of_vertices
get_edge_weight
set_edge_weight
is_directed
neighbors
minimum_degree
maximum_degree
is_regular
isomorphic_copy
permute_vertices
relabel_vertices
is_tree
is_forest
is_tournament
tree_height
number_of_triangles
is_connected
is_biconnected
is_triconnected
is_weighted
is_planar
complete_binary_tree
complete_kary_tree
prism_graph
antiprism_graph
star_graph
wheel_graph
grid_graph
torus_grid_graph
web_graph
cartesian_product
tensor_product
path_graph
is_eulerian
kneser_graph
odd_graph
highlight_vertex
highlight_edges
highlight_trail
highlight_subgraph
disjoint_union
graph_union
graph_join
graph_equal
reverse_graph
interval_graph
subdivide_edges
graph_power
vertex_distance
shortest_path
allpairs_distance
graph_diameter
dijkstra
bellman_ford
topologic_sort topological_sort
is_acyclic
is_clique
maximum_clique
clique_number
clique_cover
clique_cover_number
chromatic_number
maximum_independent_set
independence_number
strongly_connected_components
is_strongly_connected
degree_sequence
is_graphic_sequence
sequence_graph
girth
odd_girth
is_arborescence
foldl
foldr
graph_spectrum
seidel_spectrum
graph_charpoly
is_integer_graph
spanning_tree
number_of_spanning_trees
minimal_spanning_tree
graph_rank
lowest_common_ancestor
connected
spring
tree
planar
directed
weighted
weights
bipartite
st_ordering
greedy_color
is_bipartite
plane_dual
is_vertex_colorable
set_vertex_positions
clique_stats
minimal_vertex_coloring
bipartite_matching
line_graph
transitive_closure
is_isomorphic
graph_automorphisms
canonical_labeling
minimal_edge_coloring
chromatic_index
trail2edges
traveling_salesman
is_hamiltonian
is_network
random_network
maxflow
minimum_cut
is_cut_set
acyclic
is_strongly_regular
laplacian_matrix
tutte_polynomial
flow_polynomial
chromatic_polynomial
reliability_polynomial
mycielski
edge_connectivity
vertex_connectivity
is_two_edge_connected
two_edge_connected_components
clustering_coefficient
network_transitivity
kernel_density kde
bandwidth
bins
fitdistr
bvpsolve
convex
kovacicsols
euler_lagrange
icomp
jacobi_equation
conjugate_equation
tonnetz
truncate_graph
condensation
find_cycles
kspaths
