
=============
Giacpy (Sage version)
=============

:Name: giacpy_sage
:Summary: A Cython frontend to the c++ library giac. (Computer Algebra System)
:Author: Frederic Han
:Author-email: frederic.han@imj-prg.fr
:Copyright: 2012 Frederic Han
:License:  GPL v2 or above
:Home-page: http://www.math.jussieu.fr/~han/xcas/giacpy/


This is the sage version of giacpy. Since 0.6 it is named giacpy_sage to avoid confusions

- Giacpy is an interface to the c++ giac library. This interface is built with cython, and the resulting speed is similar to giac's one.

- Giac is a general purpose Computer algebra system by Bernard Parisse released under GPLv3.

      * http://www-fourier.ujf-grenoble.fr/~parisse/giac.html
      * It is build on C and C++ libraries:
        NTL (arithmetic), GSL (numerics), GMP (big integers), MPFR (bigfloats)
      * It  provides fast  algorithms  for multivariate polynomial operations (product, GCD, factorisation) and
      * symbolic  computations: solver, simplifications, limits/series, integration, summation...
      * Linear Algebra with numerical or symbolic coefficients.


AUTHORS:

- Frederic Han (2013-09-23): initial version


EXAMPLES:

- The class Pygen is the main tool to interact from python/sage with the c++ library giac via cython.

  The initialisation of a Pygen just create an object in giac, but the mathematical computation  is not done. This class is mainly for cython users.


  Here A is a Pygen element, and it is ready for any giac function.

  ::

        sage: from giacpy_sage import *    # random
        //...
        sage: A=Pygen('2+2');A
        2+2
        sage: A.eval()
        4

  In general, you may prefer to directly create a Pygen and execute the evaluation in giac. This is exactly  the meaning of the libgiac() function.

  ::

        sage: a=libgiac('2+2');a;isinstance(a,Pygen)
        4
        True

- Most common usage of this package in sage will be with the libgiac() function. This function is just the composition of the Pygen initialisation and the evaluation of this object in giac.

  ::

        sage: from giacpy_sage import libgiac,giacsettings
        sage: x,y,z=libgiac('x,y,z');  # add some giac objects
        sage: f=(x+3*y)/(x+z+1)^2 -(x+z+1)^2/(x+3*y)
        sage: f.factor()
        (3*y-x^2-2*x*z-x-z^2-2*z-1)*(3*y+x^2+2*x*z+3*x+z^2+2*z+1)/((x+z+1)^2*(3*y+x))
        sage: f.normal()
        (-x^4-4*x^3*z-4*x^3-6*x^2*z^2-12*x^2*z-5*x^2+6*x*y-4*x*z^3-12*x*z^2-12*x*z-4*x+9*y^2-z^4-4*z^3-6*z^2-4*z-1)/(x^3+3*x^2*y+2*x^2*z+2*x^2+6*x*y*z+6*x*y+x*z^2+2*x*z+x+3*y*z^2+6*y*z+3*y)

- To obtain more hints on giacpy_sage consider the help of the :func:`libgiac<_giac>` function.

  ::

        sage: libgiac?             # doctest: +SKIP

- Some settings of giac are available via the ``giacsettings`` element. (Ex: maximal number of threads in computations, allowing probabilistic algorithms or not...

  ::

        sage: R=PolynomialRing(QQ,8,'x')
        sage: I=sage.rings.ideal.Katsura(R,8)
        sage: giacsettings.proba_epsilon=1e-15;
        sage: Igiac=libgiac(I.gens());
        sage: time Bgiac=Igiac.gbasis([R.gens()],'revlex')  # doctest: +SKIP
        Running a probabilistic check for the reconstructed Groebner basis. If successfull, error probability is less than 1e-15 and is estimated to be less than 10^-109. Use proba_epsilon:=0 to certify (this takes more time).
        Time: CPU 0.46 s, Wall: 0.50 s
        sage: giacsettings.proba_epsilon=0;
        sage: Igiac=libgiac(I.gens());
        sage: time Bgiac=Igiac.gbasis([R.gens()],'revlex') # doctest: +SKIP
        Time: CPU 2.74 s, Wall: 2.75 s

  ::

        sage: from giacpy_sage import *
        sage: x=libgiac('x');f=1/(2+sin(5*x))
        sage: f.int()
        2/5/sqrt(3)*(atan((2*tan(5*x/2)+1)/sqrt(3))+pi*floor(5*x/2/pi+1/2))
        sage: f.series(x,0,3)
        1/2-5/4*x+25/8*x^2-125/48*x^3+x^4*order_size(x)
        sage: libgiac(sqrt(5)+pi).approx(100)
        5.377660631089582934871817052010779119637787758986631545245841837718337331924013898042449233720899343


SEEALSO:

    ``libgiac``, ``giacsettings``, ``Pygen``,``loadgiacgen``



GETTING HELP:

- To obtain some help on a giac keyword use the help() method. In sage the htmlhelp() method for Pygen element is disabled. Just use the ? or .help() method.

  ::

        sage: libgiac.gcd?             # doctest: +SKIP
        "Returns the greatest common divisor of 2 polynomials of several variables or of 2 integers or of 2 rationals.
        (Intg or Poly),(Intg or Poly)
        gcd(45,75);gcd(15/7,50/9);gcd(x^2-2*x+1,x^3-1);gcd(t^2-2*t+1,t^2+t-2);gcd((x^2-1)*(y^2-1)*z^2,x^3*y^3*z+(-(y^3))*z+x^3*z-z)
        lcm,euler,modgcd,ezgcd,psrgcd,heugcd,Gcd"

- You can find full html documentation about the **giac** functions  at:

      * http://www-fourier.ujf-grenoble.fr/~parisse/giac/doc/en/cascmd_en/

      * http://www-fourier.ujf-grenoble.fr/~parisse/giac/doc/fr/cascmd_fr/

      * http://www-fourier.ujf-grenoble.fr/~parisse/giac/doc/el/cascmd_el/

      * or in :doc:`$SAGE_LOCAL/share/giac/doc/en/cascmd_en/index.html`



REMARK:

- Graphics 2D Output via qcas (the qt frontend to giac) is removed in the sage version of giacpy.





---------
Changelog
---------


   * Version 0.2: 
      - Add a comparison function to Pygen. (with coersion) 
      - Add a basic definition for most giac functions.
      - Add some help.

   * Version 0.2.1: 
      - Add __neg__ and __pos__ support for Pygen. (Ex: -pi)
      - Change __repr__ to hide too long outputs.
      - Make ** be the default printing for powers in giac.

   * Version 0.2.2: 
      - Change Pygen() to Pygen('NULL'). (Ex: rand())
      - Add direct acces to the python double value of a Pygen: a._double
      - Add  conversion to giac modulars via the operator %
      - Add  ctrl-c support during list initialisation and iteration
      - Modification of __getitem__ to allow formal variables with indexes.
      - Add htmlhelp method for Pygen objects.
      - Improve the giac initialisation of Python long integers. (basic Horner method instead of strings)
      - Improve  help(giac) and doctests
      - Add support for the slice notation with giac lists

   * Version 0.2.3: 
      - Fix Pygen() None initialisation. Add crash test and improve speed in _wrap_gen
      - Add a small Makefile
      - Add a GiacSettings class with some frontends to the cas settings.
      - Add French keywords

   * Version 0.2.4:
      - Update giac 1.1 keywords.

   * Version 0.3:
      - Add a qt output for 2d graphics via qcas.
      - Fixes for giac 1.1

   * Version 0.4:
      - Fixes for Python 3 compatibility
      - Qt/qcas can be disabled at compilation. (cf setup.py)
      - 0.4.1:
	  + add some giac keywords.
      	  + add proba_epsilon in GiacSetting. 
	  + test if the html doc is present locally, otherwise open the web doc.
      - 0.4.2:
          + add sqrtflag and complexflag in GiacSettings.
          + add methods for going back to sage: _latex_, _matrix_, _vector_, _integer_, _rational_
          + add tuple support in indexes. Ex A[1,2]
      - 0.4.3:
	  + add methods: sage, _symbolic_ (current status: ZZ, QQ, ZZ/nZZ, string. Else try raw
            string evaluation in SR) 
	  + add a giac memento in docstring
      - 0.4.4:
          + add a pari_unlock after interruptions
      - 0.4.5:
          + Fixes some try/sig_on positions due to some  giac errors returned.
      - 0.4.8:
          + Fixes to build with sage 6.8
      - 0.4.9:
          + Fix a sig_on() pb in Pygen __iter__
          + Fix evaluation PB with some symbols. (sqrt(3)/libgiac('x')).factor()
      - 0.5.0:
          + Fix a memory leak in _wrap_gen
      - 0.5.1:
          + remove __init__.py (gives pb with cython 0.22.p5)
      - 0.5.2:
          + add giac keywords. (ex: eliminate ...)
      - 0.5.3:
          + use cythonize in setup.py for recent cython.
      - 0.5.4:
          + Doctest fixes because matrix(M,ring) is deprecated
      - 0.5.5:
          + use cysignals instead of interrupt.pxi
      - 0.5.6:
          + use of GIAC_neg to fix a broken built with cython 0.24
      - 0.6:
          + Change the module name from giacpy to giacpy_sage
      - 0.6.8:
          + add c++11 flag in extra_compil_args of setup.py to follow sagelib behaviour.
      - 0.7.0:
          + 2 doctest fixes for python3 compatibility.
      - 0.7.1:
          + Fix missing include with giac 1.5.0.75 and later versions.
