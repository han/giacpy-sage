#!/usr/bin/env python


from sage.env import SAGE_LOCAL,SAGE_SRC
import os

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

# settings for extra_compile_args and extra_link_args
# conf = {'CXXFLAGS' : [], 'LDFLAGS' : []}
# we should follow the same behaviour as the sage library. (cf sage/src/setup.py)
# trac 28082: we force c++11 in extra_compil_args  because the sage library does it.
conf = {'CXXFLAGS' : ['-std=c++11'], 'LDFLAGS' : []}

libraries=['giac']
library_dirs=[SAGE_LOCAL+'/lib']
# in sage 6.7 cimport Integer needs ccobject.h but it was moved to SAGE_SRC/sage/ext
# But in sage 6.8>= the function sage_include_directories was introduced to gives the includes.
try:
    # Sage >= 6.8
    from sage.env import sage_include_directories
except ImportError:
    # Sage < 6.8
    def sage_include_directories():
        return [
            os.path.join(SAGE_LOCAL, "include"),
            os.path.join(SAGE_LOCAL, "include", "csage"),
            os.path.join(SAGE_SRC),
            os.path.join(SAGE_SRC, "sage", "ext"),
            ]
    # (on sage 6.7 without csage gives undefined symbols _signals)
    libraries.append('csage')
###

include_dirs=sage_include_directories()
include_path=include_dirs

ext_modules=[]

ext_modules+=cythonize([Extension(
                   "giacpy_sage",                 # name of extension
                   ["giacpy_sage.pyx"], #  our Cython source
                   libraries=libraries,
                   library_dirs=library_dirs,
                   include_dirs=include_dirs,
                   extra_compile_args=conf["CXXFLAGS"],
                   extra_link_args=conf["LDFLAGS"],
                   language="c++")], include_path=include_path
)
#cmdclass={'build_ext': build_ext}



setup(


    name='giacpy_sage',
    version='0.7.1',
    description='A Cython frontend to the c++ library giac. (Computer Algebra System)',
    author='Frederic Han',
    author_email="frederic.han@imj-prg.fr",
    url='http://webusers.imj-prg.fr/~frederic.han/xcas/giacpy/',
    long_description=open('README.txt').read(),
    license='GPLv2 or above',
    ext_modules=ext_modules
    )
